//  ViewController.swift
//  Animation
//  Created by Viktoriia Skvarko


import UIKit

class FirstViewController: UIViewController {
    
    @IBOutlet weak var buttonDone: UIButton!
    @IBOutlet weak var textFieldEmail: UITextField!
    
    @IBAction func buttonAction(_ sender: UIButton) {
        
        let strEmail = textFieldEmail.text
        let proverka = "@"
        
        if strEmail!.contains(proverka) {
            textFieldEmail.layer.shadowOpacity = 0
        } else {
            textFieldEmail.layer.shadowOffset = CGSize(width: 0, height: 0)
            textFieldEmail.layer.shadowOpacity = 0.7
            textFieldEmail.layer.shadowColor = UIColor.red.cgColor
            textFieldEmail.layer.shadowRadius = 5
            
            UIView.animateKeyframes(withDuration: 0.2, delay: 0) {
                self.textFieldEmail.frame = self.textFieldEmail.frame.offsetBy(dx: 20, dy: 0)
            } completion: { completion in
                self.textFieldEmail.frame = self.textFieldEmail.frame.offsetBy(dx: -20, dy: 0)
            }  
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonDone.backgroundColor = .magenta
        buttonDone.layer.cornerRadius = 5
        buttonDone.layer.borderColor = UIColor.blue.cgColor
        buttonDone.layer.borderWidth = 0.7
        
        buttonDone.layer.shadowOffset = CGSize(width: 5, height: 5)
        buttonDone.layer.shadowOpacity = 0.7
        buttonDone.layer.shadowColor = UIColor.blue.cgColor
        buttonDone.layer.shadowRadius = 5
        
    }
    
    
    @IBAction func onSecondVC(_ sender: UIStoryboardSegue) {
        
        let popUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SecondViewController") as! SecondViewController
        
        self.addChild(popUpVC)
        popUpVC.view.frame = self.view.frame
        self.view.addSubview(popUpVC.view)
        
        popUpVC.didMove(toParent: self)
        
    }
    
}
