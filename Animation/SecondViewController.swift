//  SecondViewController.swift
//  Animation
//  Created by Viktoriia Skvarko


import UIKit

class SecondViewController: UIViewController {
    
    @IBOutlet weak var viewOutlet: UIView!
    
    @IBAction func dismisButton(_ sender: UIButton) {
        self.view.removeFromSuperview()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewOutlet.layer.cornerRadius = 10
    }
}
